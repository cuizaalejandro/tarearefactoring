var fizzbuzzController = require('../fizzbuzz')
var assert = require('assert');
var expect = require('chai').expect;
var shoul  = require('chai').should();


describe('FizzbuzzController ',function(){

    it('si ingreso  hasta el 1  deberia devolver  1',function(){
        var resultado = fizzbuzzController.fizzbuzz(1);
        var esperado = "  1";
        assert.equal(resultado, esperado);
    
    }); 

    it('si ingreso hasta el 3  deberia devolver  1 2 Fizz',function(){
        var resultado = fizzbuzzController.fizzbuzz(3);
        var esperado = "  1 2 Fizz";
        assert.equal(resultado, esperado);
     
    });

   it('si ingreso hasta el 5  deberia devolver  1 2 Fizz 4 Buzz',function(){
       var resultado = fizzbuzzController.fizzbuzz(5)
       assert.equal(resultado, "  1 2 Fizz 4 Buzz");

   }); 

   it('si ingreso 3  devuelve true caso contrario false',function(){
        var resultado = fizzbuzzController.esDivivsibleEntreTres(3)
        var esperado = true; 
        assert.equal(resultado, esperado);

    }); 
    it('si ingreso  5 devuelve true caso contrario ingreso false',function(){
        var resultado = fizzbuzzController.esDivivsibleEntreCinco(5)
        var esperado = true; 
        assert.equal(resultado, esperado);

    }); 
    it('si ingreso  15 devuelve FizzBuzz',function(){
        var resultado = fizzbuzzController.returnChain(15)
        var esperado = "FizzBuzz"; 
        assert.equal(resultado, esperado);

    }); 
    it('si ingreso hasta el 15  deberia devolver  1 2 fizz 4 buzz fizz 7 8 fizz buzz 11 fizz 13 14 fizzbuzz ',function(){
        var resultado = fizzbuzzController.fizzbuzz(15)
        assert.equal(resultado, "  1 2 Fizz 4 Buzz Fizz 7 8 Fizz Buzz 11 Fizz 13 14 FizzBuzz");
 
    }); 
    

   
});